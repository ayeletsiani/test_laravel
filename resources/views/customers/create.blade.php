@extends('layouts.app')
@section('content')


<h1> Create a new customer</h1>
<form method = 'post' action = "{{action('CustomerController@store')}}">
{{csrf_field()}} 
<div class="form-group">
    <label for="name"> Customer name </label>
    <input type = "text" class="form=control" name="name">
</div>
<div class="form-group">
    <label for="email"> Customer email </label>
    <input type = "text" class="form=control" name="email">
</div>
<div class="form-group">
    <label for="phone"> Customer phone </label>
    <input type = "text" class="form=control" name="phone">
</div>
<div class="form-group">
    <input type="submit" class="form=control" name="submit" value="Save">
</div>
</form>




@endsection