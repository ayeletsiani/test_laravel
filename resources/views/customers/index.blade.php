@extends('layouts.app')
@section('content')


<h1>List of the customers </h1> 
<a  href="{{route('customers.create')}}"> Create a new customer </a> 


<table class=table>
    <thead>  
        <tr>  
        <th>name</th> 
        <th>email</th> 
        <th>phone</th> 
        <th>Salesperson</th>
        <th>Deal Status</th>
        <th >Edit</th>
        <th>Delete</th> 
        </tr>  
    </thead>
    
    @foreach($customers as $customer)
        @if ($customer->status)
            @if ($user->id == $customer->user_id)
            <tr style="color:#32CD32; font-weight:bold;">
            @else 
            <tr style="color:#32CD32;">
            @endif
        @else 
            @if ($user->id == $customer->user_id)
            <tr style="font-weight:bold;">
            @else 
            <tr>
            @endif
        @endif
        
        
        <td>  {{$customer->name}} </td>
        <td>   {{$customer->email}}  </td>
        <td>   {{$customer->phone}}  </td>
        <td>  {{$customer->user_name}}  </td>
        <td> @if ($customer->status)  
                @else @can('manager')   
            
        <a href="{{route('customers.change_status', [$customer -> id, $customer -> status])}}" > Deal Closed</a>
        @endcan  </td> @endif
       
        <td>  <a href = "{{route('customers.edit',$customer -> id)}}" >  Edit the customer info </a></td>
        
        @can('manager')     
          <td>  <a href="{{route('delete', $customer->id)}}"> Delete</a>  </td>
       @endcan
       @cannot('manager')     
          <td>   Delete </td>
       @endcannot
        </tr>
   
    @endforeach
 </table> 
  

@endsection

