<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::find($id);
        $customers = Customer::all();
            return view('customers.index', compact('customers','user'));
        /*    return view('tasks.index', ['tasks' =>$tasks] ); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $id= Auth::id();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $user = User::find($id);
        $customer->user_name = $user->name;
        $customer->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /* * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        
        $customer = Customer::find($id);
        $id= Auth::id();
        $user_id =  $customer ->user_id;
        if (Gate::denies('manager')) 
        { /* אם אתה לא מנהל אז תקבל שגיאה*/
            if ($user_id != $id) 
            {
            abort(403,"Sorry, you do not hold permission to edit this customer");
            }
        
        return view('customers.edit',compact('customer'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $customer = Customer::find($id);
        $customer ->update($request->all());    
        return redirect('customers');
    
        
        
    }

    public function change_status($id,Request $request)
    {   
        if (Gate::denies('manager')) { /* אם אתה לא מנהל אז תקבל שגיאה*/
            abort(403,"Sorry you are not allowed to change customer status");
        }
        $customer = Customer::find($id);
        $customer ->status = 1; 
        $customer ->update($request->all());  
        return redirect('customers');
       
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) { /* אם אתה לא מנהל אז תקבל שגיאה */
           abort(403,"Sorry you are not allowed to delete customers"); 
        }
         
        $customer = Customer::find($id);
        $customer ->delete();
    
        return redirect('customers');
    }
}
