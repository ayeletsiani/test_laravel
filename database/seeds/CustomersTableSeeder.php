<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => 'Adam',
                'email' => 'a@a.com',
                'phone' => '05411223344',
                'user_id' => 1,
                'user_name' => 'aa',
                'status'=> 0,
                'created_at' =>date('Y-m-d G:i:s'),
            ], 
            [
                'name' => 'Mosha',
                'email' => 'm@m.com',
                'phone' => '05455667788',
                'user_id' => 2,
                'user_name' => 'bb',
                'status'=> 0,
                'created_at' =>date('Y-m-d G:i:s'),
            ], 
            [
                'name' => 'Tush',
                'email' => 't@t.com',
                'phone' => '05411223355',
                'user_id' => 2,
                'user_name' => 'bb',
                'status'=> 0,
                'created_at' =>date('Y-m-d G:i:s'),
            ], 
            [
                'name' => 'Roni',
                'email' => 'r@r.com',
                'phone' => '05411223344',
                'user_id' => 2,
                'user_name' => 'bb',
                'status'=> 0,
                'created_at' =>date('Y-m-d G:i:s'),
            ], 
            [
                'name' => 'Ayelet',
                'email' => 'y@y.com',
                'phone' => '05411223344',
                'user_id' => 3,
                'user_name' => 'cc',
                'status'=> 0,
                'created_at' =>date('Y-m-d G:i:s'),
            ], 
            [
                'name' => 'Michal',
                'email' => 'i@i.com',
                'phone' => '05411223344',
                'user_id' => 1,
                'user_name' => 'aa',
                'status'=> 0,
                'created_at' =>date('Y-m-d G:i:s'),
            ], 
           



        ]);
    }
}
