<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('customers/delete/{id}', 'CustomerController@destroy')->name('delete');
Route::resource('customers', 'CustomerController')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('customers/status/{id}/{status}', 'CustomerController@change_status')->name('customers.change_status');
Route::get('customers/{id}', 'CustomerController@show_name')->name('customers.show_name');